package ru.t1.shipilov.tm.command.system;

import ru.t1.shipilov.tm.api.service.ICommandService;
import ru.t1.shipilov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
