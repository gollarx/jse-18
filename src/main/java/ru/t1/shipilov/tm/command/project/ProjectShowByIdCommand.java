package ru.t1.shipilov.tm.command.project;

import ru.t1.shipilov.tm.model.Project;
import ru.t1.shipilov.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends  AbstractProjectCommand {

    private final String NAME = "project-show-by-id";

    private final String DESCRIPTION = "Show project by Id.";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findOneById(id);
        showProject(project);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
