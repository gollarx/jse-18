package ru.t1.shipilov.tm.command.task;

import ru.t1.shipilov.tm.enumerated.Sort;
import ru.t1.shipilov.tm.model.Task;
import ru.t1.shipilov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    private final String NAME = "task-list";

    private final String DESCRIPTION = "Show task list.";

    @Override
    public void execute() {
        System.out.println("[SHOW TASKS]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> tasks = getTaskService().findAll(sort);
        renderTasks(tasks);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
